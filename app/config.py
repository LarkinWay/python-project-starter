
import logging
import logging.config
import logging.handlers
from ConfigParser import SafeConfigParser



logging.config.fileConfig('logging.ini', disable_existing_loggers=0)
parser = SafeConfigParser()
parser.read('config.ini')

class amqp:
    
    CLOUDAMQP_URL = parser.get('amqp', 'url')



class db:

    NAME = parser.get('db', 'name')
    HOST = parser.get('db', 'host')
    PORT = parser.get('db', 'port')
    USER = parser.get('db', 'user')
    PASSWORD = parser.get('db', 'password')






